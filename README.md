# **Educational practice**
1. Topic: Choosing an optimal weather forecast source for training models taking into account external factors of the organization.
2. Purpose: To reduce the error of the predicted values ​​of the models in the problems of power engineering by using the predicted values ​​of the weather from the optimal resource.
3. Main tasks:
	1. To investigate resources that provide predictive values ​​based on the requirements of the organization;
	2. To formulate a number of criteria for choosing a source of predicted values ​​for the preparation of predictive models in energy problems;
	3. To conduct a series of experiments with calculating the forecast accuracy using the MAE, MAPE, RMSE metrics;
	4. To calculate the accuracy of forecast of machine learning regression models with data from different sources and different accuracy;
	5. To formulate of a final recommendation based on the accuracy of forecasts in energy problems;
	6. To prepare a report on the completion of the internship.
4. Main stages of the work:
	1. Research of available resources providing forecast weather values;
	2. Calculation of errors in the accuracy of forecasts of weather values ​​for a number of European cities by the metrics MAE, MAPE, RMSE;
	3. Assessment of the dependence of the accuracy of forecasts in energy problems on the accuracy of the forecast weather values;
	4. Analysis and provision of recommendations for choosing the optimal source of forecast weather values;
	5. Formation of a report on the passage of practice.
#### Completed by: student of group IVT-261 Rublev A.A.
#### Checked by: head Grebnev V.I.
